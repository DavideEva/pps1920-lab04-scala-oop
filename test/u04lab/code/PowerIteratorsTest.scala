package u04lab.code

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u04lab.code.Lists._
import u04lab.code.Optionals._

class PowerIteratorsTest {

  val factory = new PowerIteratorsFactoryImpl()

  @Test
  def testIncremental() {
    val pi = factory.incremental(5,_+2); // pi produce 5,7,9,11,13,...
    assertEquals(Option.of(5), pi.next());
    assertEquals(Option.of(7), pi.next());
    assertEquals(Option.of(9), pi.next());
    assertEquals(Option.of(11), pi.next());
    assertEquals(List.Cons(5, List.Cons(7, List.Cons(9, List.Cons(11,List.Nil())))), pi.allSoFar()); // elementi già prodotti
    for (_ <- 0 until 10) {
      pi.next(); // procedo in avanti per un po'...
    }
    assertEquals(Option.of(33), pi.next()); // sono arrivato a 33

    val prb = factory.randomBooleans(10)
    assertNotEquals(Option.empty[Boolean], prb.next())
    for (_ <- 0 to 10) {
      prb.next()
    }
    assertEquals(Option.empty[Boolean], prb.next());
  }

  @Test
  def testListFactory() {
    val lf = factory.listFactory(1, -7, 25, 42)
    assertEquals(List.Cons(1, List.Cons(-7, List.Cons(25, List.Cons(42,List.Nil())))), lf)
  }
}