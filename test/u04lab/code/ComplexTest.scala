package u04lab.code

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class ComplexTest {

  @Test
  def TryComplex(): Unit = {
    val a = Array(Complex(10,20), Complex(1,1), Complex(7,0))
    val c = a(0) + a(1) + a(2)
    assertEquals(c, Complex(18.0, 21.0)) // (ComplexImpl(18.0,21.0),18.0,21.0)
    val c2 = a(0) * a(1)
    assertEquals(c2, Complex(-10, 30)) // (ComplexImpl(-10.0,30.0),-10.0,30.0)
    assertNotEquals(c, c2)
    assertEquals(c, c)
  }
}
