package u04lab.code

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u04lab.code.Lists.List.Cons
import u04lab.code.Lists._
import u04lab.code.Optionals._

class StudentTest {

  @Test
  def testStudent(): Unit = {
    val cPPS = Course("PPS","Viroli")
    val cPCD = Course("PCD","Ricci")
    val cSDR = Course("SDR","D'Angelo")
    val s1 = Student("mario",2015)
    val s2 = Student("gino",2016)
    val s3 = Student("rino") //defaults to 2017
    s1.enrolling(cPPS)
    s1.enrolling(cPCD)
    s2.enrolling(cPPS)
    s3.enrolling(cPPS)
    s3.enrolling(cPCD)
    s3.enrolling(cSDR)
    assertEquals(s1.courses, Cons("PCD" ,Cons( "PPS" , List.nil)))
    assertEquals(s2.courses, Cons( "PPS" , List.nil))
    assertEquals(s3.courses, Cons("SDR",Cons("PCD",Cons("PPS",List.nil))))

    assertTrue(s1.hasTeacher("Ricci"))

    var c1: Course = Course("c1", "Viroli")

    assertTrue(List.Cons(cPPS, List.Cons(c1, List.nil)) match {
      case sameTeacher(t) => true
      case _ => false
    })

    assertFalse(List.Cons(cPPS, List.Cons(c1, List.Cons(cPCD, List.nil))) match {
      case sameTeacher(t) => true
      case _ => false
    })
  }
}
