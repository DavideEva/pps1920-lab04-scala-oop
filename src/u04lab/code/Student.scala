package u04lab.code

import u04lab.code.Lists._ // import custom List type (not the one in Scala stdlib)

trait Student {
  def name: String
  def year: Int
  def enrolling(course: Course): Unit // the student participates to a Course
  def enrolling(courses: Course*): Unit
  def courses: List[String] // names of course the student participates to
  def hasTeacher(teacher: String): Boolean // is the student participating to a course of this teacher?
}

trait Course {
  def name: String
  def teacher: String
}

object Student {
  def apply(name: String, year: Int = 2017): Student = StudentImpl(name, year)
  private case class StudentImpl(
    name: String,
    year: Int
  ) extends Student {
    private var listCourses: List[Course] = List.nil

    override def enrolling(course: Course): Unit = listCourses = List.Cons(course, listCourses)

    override def courses: List[String] = List.map(listCourses)(_.name)

    override def hasTeacher(teacher: String): Boolean = List.filter(listCourses)(_.teacher == teacher) != List.nil
    //List.contains(List.map(listCourses)(_.teacher))(teacher)

    override def enrolling(courses: Course*): Unit = for (c <- courses)List.Cons(c, listCourses)
  }
}

object Course {
  def apply(name: String, teacher: String): Course = CourseImpl(name, teacher)

  def unapply(arg: Course): Option[(String, String)] = Some((arg.name, arg.teacher))

  private case class CourseImpl(
    name: String,
    teacher: String
  ) extends Course
}

object sameTeacher {
  @scala.annotation.tailrec
  def unapply(arg: List[Course]): Option[String] = arg match {
    case List.Cons(Course(_, t1), p @ List.Cons(Course(_, t2), _)) if t1 == t2 => sameTeacher.unapply(p)
    case List.Cons(Course(_, t), List.Nil()) => Some(t)
    case _ => None
  }

  /*
   * Versione non tailrec, ma più compatta
    def unapply(arg: List[Course]): Option[String] = arg match {
      case List.Cons(Course(_, t1), sameTeacher(t2)) if t1 == t2 => Some(t1)
      case List.Cons(Course(_, t), List.Nil()) => Some(t)
      case _ => None
    }
   */
}

/** Hints:
  * - simply implement Course, e.g. with a case class
  * - implement Student with a StudentImpl keeping a private Set of courses
  * - try to implement in StudentImpl method courses with map
  * - try to implement in StudentImpl method hasTeacher with map and find
  * - check that the two println above work correctly
  * - refactor the code so that method enrolling accepts a variable argument Course*
  */
