package u04lab.code

import Optionals._
import Lists._
import Streams._

import scala.util.Random

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]): PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
  def listFactory[A](elements: A*): List[A]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = PowerIterator(Stream.iterate(start)(successive))

  override def fromList[A](list: List[A]): PowerIterator[A] = PowerIterator(Stream.fromList(list))

  override def randomBooleans(size: Int): PowerIterator[Boolean] = PowerIterator(Stream.take(Stream.iterate(Random.nextBoolean())(_ => Random.nextBoolean()))(size))

  override def listFactory[A](elements: A*): List[A] = {
    var list = List.nil[A]
    for (e <- elements.reverse){
      list = List.Cons(e, list)
    }
    list
  }
}

object PowerIterator {
  def apply[A](stream: Stream[A]): PowerIterator[A] = PowerIteratorImpl(stream)

  private case class PowerIteratorImpl[A](var stream: Stream[A]) extends PowerIterator[A] {

    private var list: List[A] = List.Nil[A]()

    override def next(): Option[A] = stream match {
      case Stream.Cons(head, tail) =>
        list = List.Cons(head(), list)
        stream = tail()
        Option.Some(head())
      case _ => Option.None[A]()
    }

    override def allSoFar(): List[A] = List.reverse(list)

    override def reversed(): PowerIterator[A] = new PowerIteratorsFactoryImpl().fromList(list)
  }
}